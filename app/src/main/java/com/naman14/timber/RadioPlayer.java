package com.naman14.timber;

import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.appthemeengine.Config;
import com.naman14.timber.activities.RadioPlayingActivity;
import com.naman14.timber.utils.Helpers;
import com.naman14.timber.utils.TimberUtils;
import com.naman14.timber.widgets.PlayPauseDrawable;

import net.steamcrafted.materialiconlib.MaterialIconView;

/**
 * Created by Mohammed on 28/10/2017.
 */

public class RadioPlayer {
    ImageView albumart;
    MaterialIconView previous, next;
    PlayPauseDrawable playPauseDrawable = new PlayPauseDrawable();
    FloatingActionButton playPauseFloating;
    String ateKey;
    int accentColor;
    String mLinkRadio;
    MediaPlayer mMediaPlayer;

    RadioPlayingActivity mRadioPlayingActivity;

    public RadioPlayer(RadioPlayingActivity radioPlayingActivity, View view, String linkRadio) {
        mRadioPlayingActivity = radioPlayingActivity;
        ateKey = Helpers.getATEKey(mRadioPlayingActivity);
        accentColor = Config.accentColor(mRadioPlayingActivity, ateKey);
        mLinkRadio = linkRadio;
        mMediaPlayer = new MediaPlayer();
        setSongDetails(view);
    }

    private final View.OnClickListener mFLoatingButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            playPauseDrawable.transformToPlay(true);
            playPauseDrawable.transformToPause(true);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    MusicPlayer.playOrPause(mLinkRadio);
                }
            }, 250);
        }
    };

    public void setSongDetails(View view) {

        albumart = view.findViewById(R.id.album_art);
        next = view.findViewById(R.id.next);
        previous = view.findViewById(R.id.previous);
        playPauseFloating = view.findViewById(R.id.playpausefloating);

        if (playPauseFloating != null) {
            playPauseDrawable.setColorFilter(TimberUtils.getBlackWhiteColor(accentColor), PorterDuff.Mode.MULTIPLY);
            playPauseFloating.setImageDrawable(playPauseDrawable);
            if (MusicPlayer.isPlayingUri())
                playPauseDrawable.transformToPause(false);
            else playPauseDrawable.transformToPlay(false);
        }

        setSongDetails();

    }

    private void setSongDetails() {
        updateSongDetails();

        if (next != null) {
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mRadioPlayingActivity, "next", Toast.LENGTH_SHORT).show();
                        }
                    }, 200);

                }
            });
        }
        if (previous != null) {
            previous.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mRadioPlayingActivity, "prev", Toast.LENGTH_SHORT).show();
                        }
                    }, 200);

                }
            });
        }

        if (playPauseFloating != null)
            playPauseFloating.setOnClickListener(mFLoatingButtonListener);

    }

    public void updateSongDetails() {
        if (playPauseFloating != null)
            updatePlayPauseFloatingButton();
    }

    public void updatePlayPauseFloatingButton() {
        if (MusicPlayer.isPlayingUri()) {
            playPauseDrawable.transformToPause(false);
        } else {
            playPauseDrawable.transformToPlay(false);
        }
    }
}
