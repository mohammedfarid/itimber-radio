package com.naman14.timber.fragments;


import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.appthemeengine.Config;
import com.naman14.timber.MusicPlayer;
import com.naman14.timber.R;
import com.naman14.timber.RadioPlayer;
import com.naman14.timber.activities.RadioPlayingActivity;
import com.naman14.timber.nowplaying.BaseNowplayingFragment;
import com.naman14.timber.utils.Helpers;
import com.naman14.timber.utils.PreferencesUtility;
import com.naman14.timber.utils.SlideTrackSwitcher;
import com.naman14.timber.utils.TimberUtils;
import com.naman14.timber.widgets.PlayPauseDrawable;

import net.steamcrafted.materialiconlib.MaterialIconView;

public class LatinoFragment extends BaseNowplayingFragment {

    private String linkLatino;
    private RadioPlayingActivity mRadioPlayingActivity;
    private RadioPlayer mRadioPlayer;

    String ateKey;
    int accentColor;


    public LatinoFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        linkLatino = "http://radio.ohmylatino.com/ohmylatino.mp3";
        mRadioPlayingActivity = (RadioPlayingActivity) getActivity();
        ateKey = Helpers.getATEKey(mRadioPlayingActivity);
        accentColor = Config.accentColor(mRadioPlayingActivity, ateKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_latino, container, false);
        mRadioPlayer = new RadioPlayer(mRadioPlayingActivity,rootView,linkLatino);
        return rootView;
    }
}
