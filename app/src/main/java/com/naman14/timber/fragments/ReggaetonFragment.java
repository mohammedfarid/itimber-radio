package com.naman14.timber.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.appthemeengine.Config;
import com.naman14.timber.R;
import com.naman14.timber.RadioPlayer;
import com.naman14.timber.activities.RadioPlayingActivity;
import com.naman14.timber.nowplaying.BaseNowplayingFragment;
import com.naman14.timber.utils.Constants;
import com.naman14.timber.utils.Helpers;
import com.naman14.timber.utils.NavigationUtils;


public class ReggaetonFragment extends BaseNowplayingFragment {

    private String linkReggaeton;
    private RadioPlayingActivity mRadioPlayingActivity;
    private RadioPlayer mRadioPlayer;

    String ateKey;
    int accentColor;

    public ReggaetonFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        linkReggaeton = "http://radio.ohmylatino.com/reggaeton.mp3";
        mRadioPlayingActivity = (RadioPlayingActivity) getActivity();
        ateKey = Helpers.getATEKey(mRadioPlayingActivity);
        accentColor = Config.accentColor(mRadioPlayingActivity, ateKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_reggaeton, container, false);
        mRadioPlayer = new RadioPlayer(mRadioPlayingActivity,rootView,linkReggaeton);
        return rootView;
    }

}
